import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os
from run_numerical_step import phloem_structure, phloem_model
import csv
from matplotlib import colors
import matplotlib.lines as mlines

numericalStartTime = datetime.now()
print('numericalStartTime = ' + str(numericalStartTime))

myFile = open('Model_results/PAPER_FIGURES/NEW_systematic_parameter_search/converged_runs.csv')

csvreader = csv.reader(myFile)

header = next(csvreader)
header = header[0:4] + [header[-1]]

data_matrix = []
for row in csvreader:
	row = [float(i) for i in row]
	data_matrix.append(row)

data_matrix = np.asarray(data_matrix)
data_matrix = data_matrix[:,[0,1,2,3,4,5,7]]

for gammas1 in [0.16, 0.08, 0.04, 0.02, 0.01]:
	reduced_matrix1 = data_matrix[data_matrix[:, 2]==gammas1] 
	for cexts3 in [0.1, 0.2, 0.4, 0.6]:
		reduced_matrix3 = reduced_matrix1[reduced_matrix1[:, 3]==cexts3]
		
		total_flux = np.array(reduced_matrix3[:,4] + reduced_matrix3[:,5])
		total_flux = total_flux.reshape(total_flux.shape[0],1)
		
		relative_matrix = np.hstack((reduced_matrix3, total_flux))
		relative_matrix[:,4] = np.divide(relative_matrix[:,4], relative_matrix[:,-1])
		relative_matrix[:,5] = np.divide(relative_matrix[:,5], relative_matrix[:,-1])
		
		x = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
		y = [0.1, 0.5, 1.0, 2.5, 5.0]
		z = np.zeros((len(x), len(y)))
		
		# For relative plots - plots bulk flow as percentage out of 100
		rows = 0
		for row in x:
			cols = 0
			for col in y:
				value = relative_matrix[relative_matrix[:, 0]==row]
				value = value[value[:,1]==col]
				
				if not value.any():
					value = float("Inf")
					z[rows,cols] = value
				else:
					z[rows,cols] = value[-1][4]
				cols = cols + 1
			rows = rows + 1
			
		''' # For absolute plots
		rows = 0
		for row in x:
			cols = 0
			for col in y:
				value = reduced_matrix3[reduced_matrix3[:, 0]==row]
				value = value[value[:,1]==col]
				if not value.any():
					value = 0 #float("Inf")
					z[rows,cols] = value
				else:
					z[rows,cols] = value[-1][-1]
				
				cols = cols + 1
			rows = rows + 1
		'''
		
		X,Y = np.meshgrid(x,y)
		Z = np.transpose(z)
		#Z_max = abs(max(Z.min(), Z.max(), key=abs))
		#Z[Z == 0] = Z_max
		
		plt.figure()
		plt.contourf(X,Y,Z, cmap='bwr_r', levels=np.linspace(0, 1, 11), extend='neither') #vmin=0, vmax=1)
		plt.colorbar()
		plt.xlabel('sigmas3', fontsize=16)
		plt.ylabel('betas3', fontsize=16)
		gammas1_line = mlines.Line2D([], [], color='black', marker='', markersize=1, label='gammas1 = '+str(gammas1))
		#cexts1_line = mlines.Line2D([], [], color='black', marker='', markersize=1, label='cexts1 = '+str(cexts1))
		cexts3_line = mlines.Line2D([], [], color='black', marker='', markersize=1, label='cexts3 = '+str(cexts3))
		plt.legend(handles=[gammas1_line, cexts3_line])
		#plt.show()
		plt.savefig('Model_results/PAPER_FIGURES/NEW_systematic_parameter_search/varied_sigma_beta_relative/contourf_' + str(gammas1) + '_' + str(cexts3) + '.svg')
		plt.close()


print('numericalRunTime = ' + str(datetime.now() - numericalStartTime))
