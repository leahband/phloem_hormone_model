### Run numerical simulations of the full Munch flow equations (paper)

import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os
from run_numerical_step import phloem_structure, phloem_model

numericalStartTime = datetime.now()
print('numericalStartTime = ' + str(numericalStartTime))

########################################################################

### Run model

# Root directory
rootdir = 'Model_results/Analytical_comparisons/' 

# phloem structure
l1 = 0.2 # length of loading/unloading zone
l2 = 1 # length of translocation zone
l3 = 1.4 # phloem length
zones = phloem_structure(l1/l2, (l3 - l1)/l2, l3/l2)

# general parameters
Munch = 0.22
tolerance = [10, -10]
du0_init = [0.1, 0.7]
sigma_steps = [0.2,0.2]
cext1_steps = [1.0, 1.0]
cext3_steps = [0.4,0.4]

# sugar parameters
betas = [0,0,1.0]
gammas = [0.16,0,0]
lambdas = [0,0,0]
sugar_parameters = [betas, gammas, lambdas]

# auxin parameters (fixed)

# Loop

for unloading in ['passive']:
	
	ba3 = 1.0
	la3 = 0
	ga3 = 0
	
	for loading in ['active','passive']:
		
		if loading == 'passive':
			ba1 = 5
			la1 = 0
			ga1 = 0
		elif loading == 'active':
			ba1 = 0
			la1 = 0
			ga1 = 0.16
		
		betaa = [ba1,0,ba3]
		gammaa = [ga1,0,ga3]
		lambdaa = [la1,0,la3]
		
		auxin_parameters = [betaa, gammaa, lambdaa]
			
		branch = 'paper_update_code/'
		filename = rootdir + branch
		os.makedirs(os.path.dirname(filename), exist_ok=True)
		
		location = os.getcwd()
		print(location)
		
		# Run simulation
		sugar_data, auxin_data, additional_data, du0_final = phloem_model(zones, Munch, tolerance, du0_init, sugar_parameters, auxin_parameters, sigma_steps, cext1_steps, cext3_steps)
		
		# Calculate fluxes
		Cext = 0
		Pext = 0
		Mu = Munch
		
		# Sugar
		betas = sugar_data[0]
		gammas = sugar_data[1]
		lambdas = sugar_data[2]
		sigmas = sugar_data[3]
		Ks = sugar_data[4]
		cs_numerical = sugar_data[6]
		hatCs_numerical = sugar_data[7]
		
		# Auxin
		betaa = auxin_data[0]
		gammaa = auxin_data[1]
		lambdaa = auxin_data[2]
		sigmaa = auxin_data[3]
		Ka = auxin_data[4]
		ca_numerical = auxin_data[6]
		hatCa_numerical = auxin_data[7]
		
		# Additional
		x_numerical = additional_data[0]
		u_numerical = additional_data[1]
		p_numerical = additional_data[2]
		v0_numerical = additional_data[3]
		
		## Calculate 0.5*gradU for comparison with v0
		x_steps = 7000
		dx = 1.4/x_steps
		grad_u = 0.5*np.gradient(u_numerical, dx)
		
		# Fluxes
		RHS_sugar = []
		RHS_auxin = []
		s_bulk = []
		s_diffusive = []
		s_active = []
		a_bulk = []
		a_diffusive = []
		a_active = []
		
		for i in range(0, len(x_numerical)):
			RHS_sugar.append(-(1-sigmas[i])*(sigmas[i]*(Cext - cs_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCs_numerical[i] + cs_numerical[i], 2) + 2*betas[i]*(hatCs_numerical[i] - cs_numerical[i]) + 2*gammas[i]*hatCs_numerical[i] - 2*lambdas[i]*cs_numerical[i])
			RHS_auxin.append(-(1-sigmaa[i])*(sigmaa[i]*(Cext - ca_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCa_numerical[i] + ca_numerical[i], 2) + 2*betaa[i]*(hatCa_numerical[i] - ca_numerical[i]) + 2*gammaa[i]*hatCa_numerical[i] - 2*lambdaa[i]*ca_numerical[i])
		
		for i in range(0, len(x_numerical)):
			s_bulk.append(-(1-sigmas[i])*(sigmas[i]*(Cext - cs_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCs_numerical[i] + cs_numerical[i], 2))
			s_diffusive.append(2*betas[i]*(hatCs_numerical[i] - cs_numerical[i]))
			s_active.append(2*gammas[i]*hatCs_numerical[i] - 2*lambdas[i]*cs_numerical[i])
			a_bulk.append(-(1-sigmaa[i])*(sigmas[i]*(Cext - cs_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCa_numerical[i] + ca_numerical[i], 2))
			a_diffusive.append(2*betaa[i]*(hatCa_numerical[i] - ca_numerical[i]))
			a_active.append(2*gammaa[i]*hatCa_numerical[i] - 2*lambdaa[i]*ca_numerical[i])
		
		flux_sum_sugar = np.trapz(RHS_sugar, x_numerical)
		flux_sum_auxin = np.trapz(RHS_auxin, x_numerical)
		
		flux_loading_sugar = np.trapz(RHS_sugar[:1001], x_numerical[:1001])     
		flux_translocation_sugar = np.trapz(RHS_sugar[1000:6000], x_numerical[1000:6000])
		flux_unloading_sugar = np.trapz(RHS_sugar[5999:], x_numerical[5999:])  
		
		flux_loading_auxin = np.trapz(RHS_auxin[:1001], x_numerical[:1001])
		flux_translocation_auxin = np.trapz(RHS_auxin[1000:6000], x_numerical[1000:6000])
		flux_unloading_auxin = np.trapz(RHS_auxin[5999:], x_numerical[5999:])
		
		s_bulk_loading = np.trapz(s_bulk[:1001], x_numerical[:1001])
		s_bulk_translocation = np.trapz(s_bulk[1000:6000], x_numerical[1000:6000])
		s_bulk_unloading = np.trapz(s_bulk[5999:], x_numerical[5999:])
		a_bulk_loading = np.trapz(a_bulk[:1001], x_numerical[:1001])
		a_bulk_translocation = np.trapz(a_bulk[1000:6000], x_numerical[1000:6000])
		a_bulk_unloading = np.trapz(a_bulk[5999:], x_numerical[5999:])
		
		s_diffusive_loading = np.trapz(s_diffusive[:1001], x_numerical[:1001])
		s_diffusive_translocation = np.trapz(s_diffusive[1000:6000], x_numerical[1000:6000])
		s_diffusive_unloading = np.trapz(s_diffusive[5999:], x_numerical[5999:])
		a_diffusive_loading = np.trapz(a_diffusive[:1001], x_numerical[:1001])
		a_diffusive_translocation = np.trapz(a_diffusive[1000:6000], x_numerical[1000:6000])
		a_diffusive_unloading = np.trapz(a_diffusive[5999:], x_numerical[5999:])
		
		s_active_loading = np.trapz(s_active[:1001], x_numerical[:1001])
		s_active_translocation = np.trapz(s_active[1000:6000], x_numerical[1000:6000])
		s_active_unloading = np.trapz(s_active[5999:], x_numerical[5999:])
		a_active_loading = np.trapz(a_active[:1001], x_numerical[:1001])
		a_active_translocation = np.trapz(a_active[1000:6000], x_numerical[1000:6000])
		a_active_unloading = np.trapz(a_active[5999:], x_numerical[5999:])
		
		# Store results 
		f = open(filename + 'sugar_simulation_data.csv', 'a')
		f.write('betas')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(betas[i]))
		f.write('\n' + 'gammas')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(gammas[i]))
		f.write('\n' + 'lambdas')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(lambdas[i]))
		f.write('\n' + 'sigmas')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(sigmas[i]))
		f.write('\n' + 'Ks')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(Ks[i]))
		f.write('\n' + 'x')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(x_numerical[i]))
		f.write('\n' + 'cs_phloem')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(cs_numerical[i]))
		f.write('\n' + 'cs_external')
		for i in range(0, len(x_numerical)):
			f.write(',' + str(hatCs_numerical[i]))
		f.write('\n')
		f.close()
		
		g = open(filename + 'auxin_simulation_data.csv', 'a')
		g.write('betaa')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(betaa[i]))
		g.write('\n' + 'gammaa')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(gammaa[i]))
		g.write('\n' + 'lambdaa')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(lambdaa[i]))
		g.write('\n' + 'sigmaa')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(sigmaa[i]))
		g.write('\n' + 'Ka')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(Ka[i]))
		g.write('\n' + 'x')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(x_numerical[i]))
		g.write('\n' + 'ca_phloem')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(ca_numerical[i]))
		g.write('\n' + 'ca_external')
		for i in range(0, len(x_numerical)):
			g.write(',' + str(hatCa_numerical[i]))
		g.write('\n')
		g.close()
		
		h = open(filename + 'additional_simulation_data.csv', 'a')
		h.write('x')
		for i in range(0, len(x_numerical)):
			h.write(',' + str(x_numerical[i]))
		h.write('\n' + 'u')
		for i in range(0, len(x_numerical)):
			h.write(',' + str(u_numerical[i]))
		h.write('\n' + 'p')
		for i in range(0, len(x_numerical)):
			h.write(',' + str(p_numerical[i]))
		h.write('\n' + 'v0')
		for i in range(0, len(x_numerical)):
			h.write(',' + str(v0_numerical[i]))
		h.write('\n' + 'gradu')
		for i in range(0, len(x_numerical)):
			h.write(',' + str(grad_u[i]))
		h.write('\n')
		h.close()
		
		m = open(filename + 'flux_simulation_data.csv', 'a')
		m.write(' , Total, Bulk, Diffusion, Active \n')
		m.write('Total_sugar,')
		m.write(str(flux_sum_sugar))
		m.write('\n' + 'Loading_sugar,')
		m.write(str(flux_loading_sugar) + ',' + str(s_bulk_loading) + ',' + str(s_diffusive_loading) + ',' + str(s_active_loading))
		m.write('\n' + 'Translocation_sugar,')
		m.write(str(flux_translocation_sugar) + ',' + str(s_bulk_translocation) + ',' + str(s_diffusive_translocation) + ',' + str(s_active_translocation))
		m.write('\n' + 'Unloading_sugar,')
		m.write(str(flux_unloading_sugar) + ',' + str(s_bulk_unloading) + ',' + str(s_diffusive_unloading) + ',' + str(s_active_unloading))
		m.write('\n' + 'Total_auxin,')
		m.write(str(flux_sum_auxin))
		m.write('\n' + 'Loading_auxin,')
		m.write(str(flux_loading_auxin) + ',' + str(a_bulk_loading) + ',' + str(a_diffusive_loading) + ',' + str(a_active_loading))
		m.write('\n' + 'Translocation_auxin,')
		m.write(str(flux_translocation_auxin) + ',' + str(a_bulk_translocation) + ',' + str(a_diffusive_translocation) + ',' + str(a_active_translocation))
		m.write('\n' + 'Unloading_auxin,')
		m.write(str(flux_unloading_auxin) + ',' + str(a_bulk_unloading) + ',' + str(a_diffusive_unloading) + ',' + str(a_active_unloading))
		m.write('\n')
		m.close()
		
		v = open(filename + 'compare_efflux.csv', 'a')
		v.write('sigma_s, beta_s, bulk, diffusive \n')
		v.write('1.0, 5.0,' + str(s_bulk_unloading) + ',' + str(s_diffusive_unloading) + '\n')

########################################################################

print('numericalRunTime = ' + str(datetime.now() - numericalStartTime))
