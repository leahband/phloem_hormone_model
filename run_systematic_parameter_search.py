### Run numerical simulations of the full Munch flow equations (paper)

import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os
from run_numerical_step import phloem_structure, phloem_model

numericalStartTime = datetime.now()
print('numericalStartTime = ' + str(numericalStartTime))

########################################################################

### Run model

# Root directory
rootdir = 'Model_results/PAPER_FIGURES/NEW_systematic_parameter_search/' 
os.makedirs(os.path.dirname(rootdir), exist_ok=True)
v = open(rootdir + 'parameter_search.csv', 'a')
v.write('sigmas3, betas3, gammas1, cexts3, bulk unloading, diffusive unloading, u(-1), bulk - diffusion \n')

it = 0

# phloem structure
l1 = 0.2 # length of loading/unloading zone
l2 = 1 # length of translocation zone
l3 = 1.4 # phloem length
zones = phloem_structure(l1/l2, (l3 - l1)/l2, l3/l2)

# loop parameters
sigmas3 = [0.4] #[0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
betas3 = [0.1] #[5.0, 2.5, 1.0, 0.5, 0.1]
gammas1 = [0.16] #[0.16, 0.08, 0.04, 0.02, 0.01]
cexts3 = [0.4] #[0.1, 0.2, 0.4, 0.6]

# fixed parameters
Munch = 0.22
tolerance = [10, -10]
du0_init = [0.35, 0.4]
betaa = [0,0,5]
gammaa = [0.08,0,0]
lambdaa = [0,0,0]
auxin_parameters = [betaa, gammaa, lambdaa]
lambdas_init = [0,0,0]
cext1_steps = [1.0, 1.0]


for a in sigmas3:
	sigma_steps = [a,0.5]
	for b in betas3:
		betas_init = [0,0,b]
		for c in gammas1:
			gammas_init = [c,0,0]
			
			sugar_parameters = [betas_init, gammas_init, lambdas_init]
				
			for f in cexts3:
				cext3_steps = [f, 0.1]
				
				# Run simulation 
				sugar_data, auxin_data, additional_data, du0_final = phloem_model(zones, Munch, tolerance, du0_init, sugar_parameters, auxin_parameters, sigma_steps, cext1_steps, cext3_steps)
				
				# Calculate fluxes
				Cext = 0
				Pext = 0
				Mu = Munch
				
				# Sugar
				betas = sugar_data[0]
				gammas = sugar_data[1]
				lambdas = sugar_data[2]
				sigmas = sugar_data[3]
				Ks = sugar_data[4]
				cs_numerical = sugar_data[6]
				hatCs_numerical = sugar_data[7]
				
				# Additional
				x_numerical = additional_data[0]
				u_numerical = additional_data[1]
				p_numerical = additional_data[2]
				
				# Fluxes
				RHS_sugar = []
				s_bulk = []
				s_diffusive = []
				s_active = []
				
				for i in range(0, len(x_numerical)):
					s_bulk.append(-(1-sigmas[i])*(sigmas[i]*(Cext - cs_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCs_numerical[i] + cs_numerical[i], 2))
					s_diffusive.append(2*betas[i]*(hatCs_numerical[i] - cs_numerical[i]))
					s_active.append(2*gammas[i]*hatCs_numerical[i] - 2*lambdas[i]*cs_numerical[i])
				
				s_bulk_unloading = np.trapz(s_bulk[6000:], x_numerical[6000:])
				s_diffusive_unloading = np.trapz(s_diffusive[6000:], x_numerical[6000:])
				s_active_unloading = np.trapz(s_active[6000:], x_numerical[6000:])
				
				# Store results 
				v = open(rootdir + 'missing_converged_runs.csv', 'a')
				v.write(str(a) + ',' + str(b) + ',' + str(c) + ',' + str(f) + ',' + str(s_bulk_unloading) + ',' + str(s_diffusive_unloading) + ',' + str(u_numerical[-1]) + ',' + str(s_bulk_unloading - s_diffusive_unloading) + '\n')
				
				it = it + 1
				print(it)

########################################################################

print('numericalRunTime = ' + str(datetime.now() - numericalStartTime))
