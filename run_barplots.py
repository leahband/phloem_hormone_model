import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os
import csv
import pandas as pd
import plotly.graph_objects as go

numericalStartTime = datetime.now()
print('numericalStartTime = ' + str(numericalStartTime))

### Find relevant folder

root = 'Model_results/PAPER_FIGURES/Finalised_figures/hormone_modelling/passive/vary_cexts3' 
branch = '/'
rootdir = root+branch
parameter_regime = 'cexts3'

loading = []
translocation = []
unloading = []
parameter_value = []

for subdirs, dirs, files in os.walk(rootdir):
	dirs.sort(reverse=False)
	for dir in dirs:
		subdir = rootdir + str(dir) + '/'
		
		parameter_value = np.append(parameter_value, dir).astype(np.float)
		
		for files in os.listdir(subdir):
			
			filename = os.path.join(subdir, files)
			## Fluxes
			if filename.endswith('flux_simulation_data.csv'):
				data = list(csv.reader(open(filename, 'r'), delimiter=','))
				data = data[2:5]
				data = np.array(data)
				keys = []
				for i in range(0, len(data)):
					keys.append(data[i][0])
				
				## Data should of length 3 with values bulk, diffusion, active
				loading = np.append(loading, data[0][2:].astype(np.float))
				translocation = np.append(translocation, data[1][2:].astype(np.float))
				unloading = np.append(unloading, data[2][2:].astype(np.float))
				
## Dataframe structure
unloading = abs(unloading)
parameter_value = sorted(list(parameter_value)*3, reverse=False)
y = int(len(parameter_value*2)/2)
z = int(len(parameter_value*2)/3)

df = pd.DataFrame(
	dict(
		parameter = parameter_value*2,
		layout = ['loading']*y + ['unloading']*y,
		mechanism = ['bulk', 'diffusion', 'active']*z,
		flux = list(loading) + list(unloading),
	)
)

fig = go.Figure()
fig.layout.font.size = 20
fig.update_layout(
    template="simple_white",
    xaxis=dict(title_text=parameter_regime),
    yaxis=dict(title_text="Sugar Flux"),
    barmode="stack",
    autosize=False,
    width=1228,
    height=462,
    margin=dict(
        l=50,
        r=50,
        b=50,
        t=50,
        pad=4
    )
)

colors = ["#2ca02c", "#ff7f0e", "#1f77b4"]

for r, c in zip(df.mechanism.unique(), colors):
    plot_df = df[df.mechanism == r]
    fig.add_trace(
        go.Bar(x=[plot_df.parameter, plot_df.layout], y=plot_df.flux, name=r, marker_color=c),
    )

#fig.show()

fig.write_image(rootdir + 'flux_comparison.svg')


