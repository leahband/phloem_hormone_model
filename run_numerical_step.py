### Run numerical simulations of the full Munch flow equations

import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os

'''
hatCs := concentration in the external pool (Cext in write-up)
Cext := concentration in the xylem (Cx in write-up)
'''

### Pipe structure
def phloem_structure(x1, x2, x3):
	zones = [x1, x2, x3]
	return zones

### Model
def phloem_model(zones, Munch, tolerance, du0_init, sugar_parameters, auxin_parameters, sigma_steps, cext1_steps, cext3_steps):
	# Define phloem structure
	x1 = zones[0]
	x2 = zones[1]
	x3 = zones[2]
	
	x_steps = 7000 
	dx = x3/x_steps
	x = np.linspace(0, x3, x_steps)
	
	# Set-up variables
	u = np.zeros(len(x))
	cs = np.zeros(len(x))
	ca = np.zeros(len(x))
	hatCs = np.zeros(len(x))
	hatCa = np.zeros(len(x))
	p = np.zeros(len(x))
	cs_pos = np.zeros(len(x))
	cs_neg = np.zeros(len(x))
	ca_pos = np.zeros(len(x))
	ca_neg = np.zeros(len(x))
	v0 = np.zeros(len(x))

	# Define parameters
	Mu = Munch
	
	# Define tolerances and initial du0 estimates
	tol_neg = tolerance[0]
	tol_pos = tolerance[1]
	du0_neg = du0_init[0]
	du0_pos = du0_init[1]
	
	# Define sigma as a function of x, and its derivative 
	
	sigmas = np.append([1]*len(np.linspace(0, x1, 1000)), np.append([1]*len(np.linspace(x1,x2,5000)), [sigma_steps[0]]*len(np.linspace(x2,x3,1000))))
	dsigmas = np.gradient(sigmas,dx)

	sigmaa = np.append([1]*len(np.linspace(0, x1, 1000)), np.append([1]*len(np.linspace(x1,x2,5000)), [sigma_steps[1]]*len(np.linspace(x2,x3,1000))))
	dsigmaa = np.gradient(sigmaa,dx)
	
	# Define external pool production terms 
	Ks = np.append([1]*len(np.linspace(0, x1, 1000)), np.append([0]*len(np.linspace(x1,x2,5000)), [-1]*len(np.linspace(x2,x3,1000))))
	Ka = np.append([1]*len(np.linspace(0, x1, 1000)), np.append([0]*len(np.linspace(x1,x2,5000)), [-1]*len(np.linspace(x2,x3,1000))))
	
	# Define loading and unloading parameters as a function of x
	betas1 = sugar_parameters[0][0]
	betas2 = sugar_parameters[0][1]
	betas3 = sugar_parameters[0][2]
	gammas1 = sugar_parameters[1][0]
	gammas2 = sugar_parameters[1][1]
	gammas3 = sugar_parameters[1][2]
	lambdas1 = sugar_parameters[2][0]
	lambdas2 = sugar_parameters[2][1]
	lambdas3 = sugar_parameters[2][2]
	
	betas = np.append([betas1]*len(np.linspace(0, x1, 1000)), np.append([betas2]*len(np.linspace(x1,x2,5000)), [betas3]*len(np.linspace(x2,x3,1000))))
	gammas = np.append([gammas1]*len(np.linspace(0, x1, 1000)), np.append([gammas2]*len(np.linspace(x1,x2,5000)), [gammas3]*len(np.linspace(x2,x3,1000))))
	lambdas = np.append([lambdas1]*len(np.linspace(0, x1, 1000)), np.append([lambdas2]*len(np.linspace(x1,x2,5000)), [lambdas3]*len(np.linspace(x2,x3,1000))))
	
	betaa1 = auxin_parameters[0][0]
	betaa2 = auxin_parameters[0][1]
	betaa3 = auxin_parameters[0][2]
	gammaa1 = auxin_parameters[1][0]
	gammaa2 = auxin_parameters[1][1]
	gammaa3 = auxin_parameters[1][2]
	lambdaa1 = auxin_parameters[2][0]
	lambdaa2 = auxin_parameters[2][1]
	lambdaa3 = auxin_parameters[2][2]
	
	betaa = np.append([betaa1]*len(np.linspace(0, x1, 1000)), np.append([betaa2]*len(np.linspace(x1,x2,5000)), [betaa3]*len(np.linspace(x2,x3,1000))))
	gammaa = np.append([gammaa1]*len(np.linspace(0, x1, 1000)), np.append([gammaa2]*len(np.linspace(x1,x2,5000)), [gammaa3]*len(np.linspace(x2,x3,1000))))
	lambdaa = np.append([lambdaa1]*len(np.linspace(0, x1, 1000)), np.append([lambdaa2]*len(np.linspace(x1,x2,5000)), [lambdaa3]*len(np.linspace(x2,x3,1000))))
	
	hatCs = np.append([cext1_steps[0]]*len(np.linspace(0, x1, 1000)), np.append([0]*len(np.linspace(x1,x2,5000)), [cext3_steps[0]]*len(np.linspace(x2,x3,1000))))
	hatCa = np.append([cext1_steps[1]]*len(np.linspace(0, x1, 1000)), np.append([0]*len(np.linspace(x1,x2,5000)), [cext3_steps[1]]*len(np.linspace(x2,x3,1000))))
	
	# Checking for opposite signs

	## Checking for negative sign
	du0 = du0_neg
	tol = tol_neg
	it = 0
	while tol > 0:
	
		it = it + 1
		
		Pext = 0
		Cext = 0
		
		u[0] = 0
		
		cs[0] = np.divide(np.multiply(np.divide(1 - sigmas[0], 2), du0) + 2*(betas[0] + gammas[0]), np.multiply(1 - np.divide(1 - sigmas[0], 2), du0) + 2*(betas[0] + lambdas[0]))
		ca[0] = np.divide(np.multiply(np.divide(1 - sigmaa[0], 2), du0) + 2*(betaa[0] + gammaa[0]), np.multiply(1 - np.divide(1 - sigmaa[0], 2), du0) + 2*(betaa[0] + lambdaa[0]))
		
		p[0] = np.multiply(np.divide(8,Mu), sigmas[0]*cs[0] - du0)
		
		u[1] = u[0] + du0*dx
		p[1] = p[0] - 8*dx*u[1]
		
		As = -np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)
		Bs = 2*u[1] - u[0] + 2*dx*(betas[1] + lambdas[1]) + np.divide((1 - sigmas[1])*dx*Mu, 16)*(p[1] - Pext) - np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)*(hatCs[1] - Cext)
		Ds = u[1]*cs[0] + 2*dx*(betas[1] + gammas[1])*hatCs[1] - (np.divide((1 - sigmas[1])*dx*Mu, 16)*(p[1] - Pext) + np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)*Cext)*hatCs[1]
		cs[1] = np.divide(Ds, Bs) - As*np.divide(np.power(Ds, 2), np.power(Bs, 3))
		
		long_term = np.multiply(np.divide((1 - sigmaa[1])*dx , 2) , sigmas[1]*(cs[1] - Cext) - np.divide(Mu,8)*(p[1] - Pext))
		num = u[1]*ca[0] + np.multiply(long_term + 2*(betaa[1] + gammaa[1])*dx, hatCa[1])
		dom = 2*u[1] - u[0] - long_term + 2*(betaa[1] + lambdaa[1])*dx
		ca[1] = np.divide(num, dom)		

		# Upwind method
		for j in range(2, len(x)):
			
			u[j] = u[j-1] + sigmas[j]*dx*(cs[j-1] - Cext) - np.divide(Mu,8)*dx*(p[j-1] - Pext)
			
			p[j] = p[j-1] - 8*dx*u[j]
			
			As = -np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)
			Bs = 2*u[j] - u[j-1] + 2*dx*(betas[j] + lambdas[j]) + np.divide((1 - sigmas[j])*dx*Mu, 16)*(p[j] - Pext) - np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)*(hatCs[j] - Cext)
			Ds = u[j]*cs[j-1] + 2*dx*(betas[j] + gammas[j])*hatCs[j] - (np.divide((1 - sigmas[j])*dx*Mu, 16)*(p[j] - Pext) + np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)*Cext)*hatCs[j]
			cs[j] = np.divide(Ds, Bs) - As*np.divide(np.power(Ds, 2), np.power(Bs, 3))
			
			long_term = np.multiply(np.divide((1 - sigmaa[j])*dx , 2) , sigmas[j]*(cs[j] - Cext) - np.divide(Mu,8)*(p[j] - Pext))
			num = u[j]*ca[j-1] + np.multiply(long_term + 2*(betaa[j] + gammaa[j])*dx, hatCa[j])
			dom = 2*u[j] - u[j-1] - long_term + 2*(betaa[j] + lambdaa[j])*dx
			ca[j] = np.divide(num, dom)
			
		# Update tolerance and  du(0)/dx estimate
		tol_old = tol
		tol = u[-1]
		du0 = du0 - 0.001
		du0_neg_o = du0
	
		# Insert break to avoid infinite loops
		if abs(tol_old) < abs(tol):
			break
		
	u_neg = u[-1]

	## Checking for positive sign
	du0 = du0_pos
	tol = tol_pos
	it = 0
	while tol < 0:
		
		it = it + 1
		
		Pext = 0
		Cext = 0
		
		u[0] = 0
		
		cs[0] = np.divide(np.multiply(np.divide(1 - sigmas[0], 2), du0) + 2*(betas[0] + gammas[0]), np.multiply(1 - np.divide(1 - sigmas[0], 2), du0) + 2*(betas[0] + lambdas[0]))
		ca[0] = np.divide(np.multiply(np.divide(1 - sigmaa[0], 2), du0) + 2*(betaa[0] + gammaa[0]), np.multiply(1 - np.divide(1 - sigmaa[0], 2), du0) + 2*(betaa[0] + lambdaa[0]))
		
		p[0] = np.multiply(np.divide(8,Mu), sigmas[0]*cs[0] - du0)
		
		u[1] = u[0] + du0*dx
		p[1] = p[0] - 8*dx*u[1]
		
		As = -np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)
		Bs = 2*u[1] - u[0] + 2*dx*(betas[1] + lambdas[1]) + np.divide((1 - sigmas[1])*dx*Mu, 16)*(p[1] - Pext) - np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)*(hatCs[1] - Cext)
		Ds = u[1]*cs[0] + 2*dx*(betas[1] + gammas[1])*hatCs[1] - (np.divide((1 - sigmas[1])*dx*Mu, 16)*(p[1] - Pext) + np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)*Cext)*hatCs[1]
		cs[1] = np.divide(Ds, Bs) - As*np.divide(np.power(Ds, 2), np.power(Bs, 3))
			
		long_term = np.multiply(np.divide((1 - sigmaa[1])*dx , 2) , sigmas[1]*(cs[1] - Cext) - np.divide(Mu,8)*(p[1] - Pext))
		num = u[1]*ca[0] + np.multiply(long_term + 2*(betaa[1] + gammaa[1])*dx, hatCa[1])
		dom = 2*u[1] - u[0] - long_term + 2*(betaa[1] + lambdaa[1])*dx
		ca[1] = np.divide(num, dom)	

		# Upwind method
		for j in range(2, len(x)):
			
			u[j] = u[j-1] + sigmas[j]*dx*(cs[j-1] - Cext) - np.divide(Mu,8)*dx*(p[j-1] - Pext)
			
			p[j] = p[j-1] - 8*dx*u[j]
			
			As = -np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)
			Bs = 2*u[j] - u[j-1] + 2*dx*(betas[j] + lambdas[j]) + np.divide((1 - sigmas[j])*dx*Mu, 16)*(p[j] - Pext) - np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)*(hatCs[j] - Cext)
			Ds = u[j]*cs[j-1] + 2*dx*(betas[j] + gammas[j])*hatCs[j] - (np.divide((1 - sigmas[j])*dx*Mu, 16)*(p[j] - Pext) + np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)*Cext)*hatCs[j]
			cs[j] = np.divide(Ds, Bs) - As*np.divide(np.power(Ds, 2), np.power(Bs, 3))
				
			long_term = np.multiply(np.divide((1 - sigmaa[j])*dx , 2) , sigmas[j]*(cs[j] - Cext) - np.divide(Mu,8)*(p[j] - Pext))
			num = u[j]*ca[j-1] + np.multiply(long_term + 2*(betaa[j] + gammaa[j])*dx, hatCa[j])
			dom = 2*u[j] - u[j-1] - long_term + 2*(betaa[j] + lambdaa[j])*dx
			ca[j] = np.divide(num, dom)
			
		# Update tolerance and  du(0)/dx estimate
		tol_old = tol
		tol = u[-1]
		du0 = du0 - 0.001
		du0_pos_o = du0
		
		# Insert break to avoid infinite loops
		if abs(tol_old) < abs(tol):
			break
			
	u_pos = u[-1]
				
	## Perform shooting method
	tol = 100
	du0_neg = du0_neg_o
	du0_pos = du0_pos_o
	
	while abs(tol) >= 1e-6:
		
		# Count number of iterations
		it = it + 1
		
		# Perform interval bisection
		du0 = np.divide(du0_neg + du0_pos, 2)
		
		Pext = 0
		Cext = 0
		
		u[0] = 0
		
		cs[0] = np.divide(np.multiply(np.divide(1 - sigmas[0], 2), du0) + 2*(betas[0] + gammas[0]), np.multiply(1 - np.divide(1 - sigmas[0], 2), du0) + 2*(betas[0] + lambdas[0]))
		ca[0] = np.divide(np.multiply(np.divide(1 - sigmaa[0], 2), du0) + 2*(betaa[0] + gammaa[0]), np.multiply(1 - np.divide(1 - sigmaa[0], 2), du0) + 2*(betaa[0] + lambdaa[0]))
		
		p[0] = np.multiply(np.divide(8,Mu), sigmas[0]*cs[0] - du0)
		
		u[1] = u[0] + du0*dx
		p[1] = p[0] - 8*dx*u[1]
		
		As = -np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)
		Bs = 2*u[1] - u[0] + 2*dx*(betas[1] + lambdas[1]) + np.divide((1 - sigmas[1])*dx*Mu, 16)*(p[1] - Pext) - np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)*(hatCs[1] - Cext)
		Ds = u[1]*cs[0] + 2*dx*(betas[1] + gammas[1])*hatCs[1] - (np.divide((1 - sigmas[1])*dx*Mu, 16)*(p[1] - Pext) + np.divide(sigmas[1]*(1 - sigmas[1])*dx, 2)*Cext)*hatCs[1]
		cs[1] = np.divide(Ds, Bs) - As*np.divide(np.power(Ds, 2), np.power(Bs, 3))
			
		long_term = np.multiply(np.divide((1 - sigmaa[1])*dx , 2) , sigmas[1]*(cs[1] - Cext) - np.divide(Mu,8)*(p[1] - Pext))
		num = u[1]*ca[0] + np.multiply(long_term + 2*(betaa[1] + gammaa[1])*dx, hatCa[1])
		dom = 2*u[1] - u[0] - long_term + 2*(betaa[1] + lambdaa[1])*dx
		ca[1] = np.divide(num, dom)	
		
		v0[0] = 0.5*(sigmas[0]*(cs[0] - Cext) - np.divide(Mu, 8)*(p[0] - Pext))
		v0[1] = 0.5*(sigmas[1]*(cs[1] - Cext) - np.divide(Mu, 8)*(p[1] - Pext))

		# Upwind method
		for j in range(2, len(x)):
			
			u[j] = u[j-1] + sigmas[j]*dx*(cs[j-1] - Cext) - np.divide(Mu,8)*dx*(p[j-1] - Pext)
			
			p[j] = p[j-1] - 8*dx*u[j]
			
			As = -np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)
			Bs = 2*u[j] - u[j-1] + 2*dx*(betas[j] + lambdas[j]) + np.divide((1 - sigmas[j])*dx*Mu, 16)*(p[j] - Pext) - np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)*(hatCs[j] - Cext)
			Ds = u[j]*cs[j-1] + 2*dx*(betas[j] + gammas[j])*hatCs[j] - (np.divide((1 - sigmas[j])*dx*Mu, 16)*(p[j] - Pext) + np.divide(sigmas[j]*(1 - sigmas[j])*dx, 2)*Cext)*hatCs[j]
			cs[j] = np.divide(Ds, Bs) - As*np.divide(np.power(Ds, 2), np.power(Bs, 3))
				
			long_term = np.multiply(np.divide((1 - sigmaa[j])*dx , 2) , sigmas[j]*(cs[j] - Cext) - np.divide(Mu,8)*(p[j] - Pext))
			num = u[j]*ca[j-1] + np.multiply(long_term + 2*(betaa[j] + gammaa[j])*dx, hatCa[j])
			dom = 2*u[j] - u[j-1] - long_term + 2*(betaa[j] + lambdaa[j])*dx
			ca[j] = np.divide(num, dom)
			
			v0[j] = 0.5*(sigmas[j]*(cs[j] - Cext) - np.divide(Mu, 8)*(p[j] - Pext))
			
		# Update tolerance and  du(0)/dx estimate
		tol_old = tol
		tol = u[-1]
		
		if u[-1] < 0:
			du0_neg = du0
		elif u[-1] > 0:
			du0_pos = du0
		else:
			break	
		
		# Insert break to avoid infinite loops
		if it > 50:
			
			print('error: iteration tolerance exceeded')
			break
			
	# Print final iteration count
	print('Iterations: ' + str(it))
	print('du0 = ' + str(du0))
	print('p(0) = ' + str(p[0]))
	print('u(0) = ' + str(u[0]))
	print('u(1) = ' + str(u[-1]))
	f = np.multiply((1 - sigmas[-1])*np.divide(cs[-1] + hatCs[-1], 2), sigmas[-1]*cs[-1] - np.divide(Mu, 8)*p[-1]) + 2*(betas[-1]+gammas[-1])*hatCs[-1] - 2*(betas[-1]+lambdas[-1])*cs[-1]
	w = np.gradient(u,dx)
	print('Check singularity: ' + str(f - cs[-1]*w[-1]))
	
	x_numerical = x
	u_numerical = u
	cs_numerical = cs
	ca_numerical = ca
	hatCs_numerical = hatCs
	hatCa_numerical = hatCa
	p_numerical = p
	v0_numerical = v0
	du0_final = du0 
	 
	sugar_data = [betas, gammas, lambdas, sigmas, Ks, x_numerical, cs_numerical, hatCs_numerical]
	auxin_data = [betaa, gammaa, lambdaa, sigmaa, Ka, x_numerical, ca_numerical, hatCa_numerical]
	additional_data = [x_numerical, u_numerical, p_numerical, v0_numerical]
	
	return sugar_data, auxin_data, additional_data, du0_final
	
########################################################################

# Run model

l1 = 0.2 # length of loading/unloading zone
l2 = 1 # length of translocation zone
l3 = 1.4 # phloem length

zones = phloem_structure(l1/l2, (l3 - l1)/l2, l3/l2)

Munch = 0.22
tolerance = [10, -10]
du0_init = [0.344, 0.347]
betas = [0,0,0.1]
gammas = [0.16,0,0]
lambdas = [0,0,0]
sugar_parameters = [betas, gammas, lambdas]
betaa = [5,0,5]
gammaa = [0.05,0,0]
lambdaa = [0,0,0]
auxin_parameters = [betaa, gammaa, lambdaa]
sigma_steps = [0.4,1]
cext1_step = [1.0,1.0]
cext3_step = [0.4, 0.1]

# Standard run

sugar_data, auxin_data, additional_data, du0_final = phloem_model(zones, Munch, tolerance, du0_init, sugar_parameters, auxin_parameters, sigma_steps, cext1_step, cext3_step)

# Incoming and outgoing flux 

# Fixed parameters
Cext = 0
Pext = 0
Mu = Munch

## Calculate fluxes
# Sugar
betas = sugar_data[0]
gammas = sugar_data[1]
lambdas = sugar_data[2]
sigmas = sugar_data[3]
Ks = sugar_data[4]
cs_numerical = sugar_data[6]
hatCs_numerical = sugar_data[7]
# Auxin
betaa = auxin_data[0]
gammaa = auxin_data[1]
lambdaa = auxin_data[2]
sigmaa = auxin_data[3]
Ka = auxin_data[4]
ca_numerical = auxin_data[6]
hatCa_numerical = auxin_data[7]
# Additional
x_numerical = additional_data[0]
u_numerical = additional_data[1]
p_numerical = additional_data[2]
v0_numerical = additional_data[3]
## Calculate 0.5*gradU for comparison with v0
x_steps = 7000 #10000
dx = 1.4/x_steps
grad_u = 0.5*np.gradient(u_numerical, dx)

# Sugar - attempt two (sum RHS of (3.39b))

RHS_sugar = []
RHS_auxin = []
s_bulk = []
s_diffusive = []
s_active = []
a_bulk = []
a_diffusive = []
a_active = []


for i in range(0, len(x_numerical)):
	RHS_sugar.append(-(1-sigmas[i])*(sigmas[i]*(Cext - cs_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCs_numerical[i] + cs_numerical[i], 2) + 2*betas[i]*(hatCs_numerical[i] - cs_numerical[i]) + 2*gammas[i]*hatCs_numerical[i] - 2*lambdas[i]*cs_numerical[i])
	RHS_auxin.append(-(1-sigmaa[i])*(sigmaa[i]*(Cext - ca_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCa_numerical[i] + ca_numerical[i], 2) + 2*betaa[i]*(hatCa_numerical[i] - ca_numerical[i]) + 2*gammaa[i]*hatCa_numerical[i] - 2*lambdaa[i]*ca_numerical[i])

for i in range(0, len(x_numerical)):
	s_bulk.append(-(1-sigmas[i])*(sigmas[i]*(Cext - cs_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCs_numerical[i] + cs_numerical[i], 2))
	s_diffusive.append(2*betas[i]*(hatCs_numerical[i] - cs_numerical[i]))
	s_active.append(2*gammas[i]*hatCs_numerical[i] - 2*lambdas[i]*cs_numerical[i])
	a_bulk.append(-(1-sigmaa[i])*(sigmaa[i]*(Cext - ca_numerical[i]) - np.divide(Mu, 8)*(Pext - p_numerical[i]))*np.divide(hatCa_numerical[i] + ca_numerical[i], 2))
	a_diffusive.append(2*betaa[i]*(hatCa_numerical[i] - ca_numerical[i]))
	a_active.append(2*gammaa[i]*hatCa_numerical[i] - 2*lambdaa[i]*ca_numerical[i])

flux_sum_sugar = np.trapz(RHS_sugar, x_numerical)
flux_sum_auxin = np.trapz(RHS_auxin, x_numerical)

flux_loading_sugar = np.trapz(RHS_sugar[:1001], x_numerical[:1001])     
flux_translocation_sugar = np.trapz(RHS_sugar[1000:6001], x_numerical[1000:6001])
flux_unloading_sugar = np.trapz(RHS_sugar[6000:], x_numerical[6000:])   

flux_loading_auxin = np.trapz(RHS_auxin[:1001], x_numerical[:1001])
flux_translocation_auxin = np.trapz(RHS_auxin[1000:6001], x_numerical[1000:6001])
flux_unloading_auxin = np.trapz(RHS_auxin[6000:], x_numerical[6000:])

s_bulk_loading = np.trapz(s_bulk[:1001], x_numerical[:1001])
s_bulk_translocation = np.trapz(s_bulk[1000:6001], x_numerical[1000:6001])
s_bulk_unloading = np.trapz(s_bulk[6000:], x_numerical[6000:])
a_bulk_loading = np.trapz(a_bulk[:1001], x_numerical[:1001])
a_bulk_translocation = np.trapz(a_bulk[1000:6001], x_numerical[1000:6001])
a_bulk_unloading = np.trapz(a_bulk[6000:], x_numerical[6000:])

s_diffusive_loading = np.trapz(s_diffusive[:1001], x_numerical[:1001])
s_diffusive_translocation = np.trapz(s_diffusive[1000:6001], x_numerical[1000:6001])
s_diffusive_unloading = np.trapz(s_diffusive[6000:], x_numerical[6000:])
a_diffusive_loading = np.trapz(a_diffusive[:1001], x_numerical[:1001])
a_diffusive_translocation = np.trapz(a_diffusive[1000:6001], x_numerical[1000:6001])
a_diffusive_unloading = np.trapz(a_diffusive[6000:], x_numerical[6000:])

s_active_loading = np.trapz(s_active[:1001], x_numerical[:1001])
s_active_translocation = np.trapz(s_active[1000:6001], x_numerical[1000:6001])
s_active_unloading = np.trapz(s_active[6000:], x_numerical[6000:])
a_active_loading = np.trapz(a_active[:1001], x_numerical[:1001])
a_active_translocation = np.trapz(a_active[1000:6001], x_numerical[1000:6001])
a_active_unloading = np.trapz(a_active[6000:], x_numerical[6000:])

## Create folder path for all outputs
rootdir = 'Model_results/PAPER_FIGURES/NEW_systematic_parameter_search/' 
branch = 'contour_runs/gammas1_' + str(gammas[0]) + '_cexts3_' + str(cext3_step[0]) + '/(' + str(sigma_steps[0]) + ',' + str(betas[-1]) + ')/' 
filename = rootdir + branch
os.makedirs(os.path.dirname(filename), exist_ok=True)

## Store results 

f = open(filename + 'sugar_simulation_data.csv', 'a')
f.write('betas')
for i in range(0, len(x_numerical)):
	f.write(',' + str(betas[i]))
f.write('\n' + 'gammas')
for i in range(0, len(x_numerical)):
	f.write(',' + str(gammas[i]))
f.write('\n' + 'lambdas')
for i in range(0, len(x_numerical)):
	f.write(',' + str(lambdas[i]))
f.write('\n' + 'sigmas')
for i in range(0, len(x_numerical)):
	f.write(',' + str(sigmas[i]))
f.write('\n' + 'Ks')
for i in range(0, len(x_numerical)):
	f.write(',' + str(Ks[i]))
f.write('\n' + 'x')
for i in range(0, len(x_numerical)):
	f.write(',' + str(x_numerical[i]))
f.write('\n' + 'cs_phloem')
for i in range(0, len(x_numerical)):
	f.write(',' + str(cs_numerical[i]))
f.write('\n' + 'cs_external')
for i in range(0, len(x_numerical)):
	f.write(',' + str(hatCs_numerical[i]))
f.write('\n')
f.close()

g = open(filename + 'auxin_simulation_data.csv', 'a')
g.write('betaa')
for i in range(0, len(x_numerical)):
	g.write(',' + str(betaa[i]))
g.write('\n' + 'gammaa')
for i in range(0, len(x_numerical)):
	g.write(',' + str(gammaa[i]))
g.write('\n' + 'lambdaa')
for i in range(0, len(x_numerical)):
	g.write(',' + str(lambdaa[i]))
g.write('\n' + 'sigmaa')
for i in range(0, len(x_numerical)):
	g.write(',' + str(sigmaa[i]))
g.write('\n' + 'Ka')
for i in range(0, len(x_numerical)):
	g.write(',' + str(Ka[i]))
g.write('\n' + 'x')
for i in range(0, len(x_numerical)):
	g.write(',' + str(x_numerical[i]))
g.write('\n' + 'ca_phloem')
for i in range(0, len(x_numerical)):
	g.write(',' + str(ca_numerical[i]))
g.write('\n' + 'ca_external')
for i in range(0, len(x_numerical)):
	g.write(',' + str(hatCa_numerical[i]))
g.write('\n')
g.close()

h = open(filename + 'additional_simulation_data.csv', 'a')
h.write('x')
for i in range(0, len(x_numerical)):
	h.write(',' + str(x_numerical[i]))
h.write('\n' + 'u')
for i in range(0, len(x_numerical)):
	h.write(',' + str(u_numerical[i]))
h.write('\n' + 'p')
for i in range(0, len(x_numerical)):
	h.write(',' + str(p_numerical[i]))
h.write('\n' + 'v0')
for i in range(0, len(x_numerical)):
	h.write(',' + str(v0_numerical[i]))
h.write('\n' + 'gradu')
for i in range(0, len(x_numerical)):
	h.write(',' + str(grad_u[i]))
h.write('\n')
h.close()

m = open(filename + 'flux_simulation_data.csv', 'a')
m.write(' , Total, Bulk, Diffusion, Active \n')
m.write('Total_sugar,')
m.write(str(flux_sum_sugar))
m.write('\n' + 'Loading_sugar,')
m.write(str(flux_loading_sugar) + ',' + str(s_bulk_loading) + ',' + str(s_diffusive_loading) + ',' + str(s_active_loading))
m.write('\n' + 'Translocation_sugar,')
m.write(str(flux_translocation_sugar) + ',' + str(s_bulk_translocation) + ',' + str(s_diffusive_translocation) + ',' + str(s_active_translocation))
m.write('\n' + 'Unloading_sugar,')
m.write(str(flux_unloading_sugar) + ',' + str(s_bulk_unloading) + ',' + str(s_diffusive_unloading) + ',' + str(s_active_unloading))
m.write('\n' + 'Total_auxin,')
m.write(str(flux_sum_auxin))
m.write('\n' + 'Loading_auxin,')
m.write(str(flux_loading_auxin) + ',' + str(a_bulk_loading) + ',' + str(a_diffusive_loading) + ',' + str(a_active_unloading))
m.write('\n' + 'Translocation_auxin,')
m.write(str(flux_translocation_auxin) + ',' + str(a_bulk_translocation) + ',' + str(a_diffusive_translocation) + ',' + str(a_active_translocation))
m.write('\n' + 'Unloading_auxin,')
m.write(str(flux_unloading_auxin) + ',' + str(a_bulk_unloading) + ',' + str(a_diffusive_unloading) + ',' + str(a_active_unloading))
m.write('\n')
m.close()

v = open(filename + 'compare_efflux.csv', 'a')
v.write('sigma_s, beta_s, bulk, diffusive \n')
v.write('1.0, 5.0,' + str(s_bulk_unloading) + ',' + str(s_diffusive_unloading) + '\n')

########################################################################
