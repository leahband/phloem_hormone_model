### Takes csv files of phloem model runs and returns our figures
### Figures save as svg files

import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os
import csv

numericalStartTime = datetime.now()
print('numericalStartTime = ' + str(numericalStartTime))

### Find relevant folder

root = 'Model_results/Analytical_comparisons/' 
branch = 'paper_update_code/'
rootdir = root+branch
parameter_regime = 'Munch'

for subdirs, dirs, files in os.walk(rootdir):
	dirs.sort(reverse=False)
	for dir in dirs:
		subdir = rootdir + str(dir) + '/'
		for files in os.listdir(subdir):
			filename = os.path.join(subdir, files)
			## Sugar
			if filename.endswith('sugar_simulation_data.csv'):
				data = np.array(list(csv.reader(open(filename, 'r'), delimiter=',')))
				keys = []
				for i in range(0, len(data)):
					keys.append(data[i][0])
				
				betas = data[0][1:].astype(np.float)
				gammas = data[1][1:].astype(np.float)
				lambdas = data[2][1:].astype(np.float)
				sigmas = data[3][1:].astype(np.float)
				Ks = data[4][1:].astype(np.float)
				x_numerical = data[5][1:].astype(np.float)
				cs_numerical = data[6][1:].astype(np.float)
				cs_ext = data[7][1:].astype(np.float)
				
				# Plots
				plt.figure(1)
				plt.plot(x_numerical, cs_numerical, label=str(dir), color='y')
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Sugar concentration, $c_{s}$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'phloem_sugar.svg')
				plt.close()
				
				plt.figure(2)
				plt.plot(x_numerical, cs_ext, label=str(dir), color='y')
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Sugar concentration, $c_{s, ext}$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'external_sugar.svg')
				plt.close()
				
				plt.figure(3)
				plt.plot(x_numerical, cs_numerical, label=str(dir))#, color='y')
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Sugar concentration, $c_{s}$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(rootdir + 'comparison_sugar.svg')
				
				continue
				
			## Auxin
			elif filename.endswith('auxin_simulation_data.csv'):
				data = np.array(list(csv.reader(open(filename, 'r'), delimiter=',')))
				keys = []
				for i in range(0, len(data)):
					keys.append(data[i][0])
				
				betaa = data[0][1:].astype(np.float)
				gammaa = data[1][1:].astype(np.float)
				lambdaa = data[2][1:].astype(np.float)
				sigmaa = data[3][1:].astype(np.float)
				Ka = data[4][1:].astype(np.float)
				x_numerical = data[5][1:].astype(np.float)
				ca_numerical = data[6][1:].astype(np.float)
				ca_ext = data[7][1:].astype(np.float)
				
				# Plots
				plt.figure(4)
				plt.plot(x_numerical, ca_numerical, label=str(dir), color='r')
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Auxin concentration, $c_{a}$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'phloem_auxin.svg')
				plt.close()
				
				plt.figure(5)
				plt.plot(x_numerical, ca_ext, label=str(dir), color='r')
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Auxin concentration, $c_{a, ext}$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'external_auxin.svg')
				plt.close()
				
				plt.figure(6)
				plt.plot(x_numerical, ca_numerical, label=str(dir))#, color='y')
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Auxin concentration, $c_{a}$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(rootdir + 'comparison_auxin.svg')
				
				continue
				
			## Additional plots
			elif filename.endswith('additional_simulation_data.csv'):				
				data = np.array(list(csv.reader(open(filename, 'r'), delimiter=',')))
				keys = []
				for i in range(0, len(data)):
					keys.append(data[i][0])
				
				x_numerical = data[0][1:].astype(np.float)
				u_numerical = data[1][1:].astype(np.float)
				p_numerical = data[2][1:].astype(np.float)
				v0_numerical = data[3][1:].astype(np.float)
				gradU_numerical = data[4][1:].astype(np.float)
				
				plt.figure(7)
				plt.plot(x_numerical, u_numerical, label=str(dir))
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Velocity, $u$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'phloem_velocity.svg')
				plt.close()
				
				plt.figure(8)
				plt.plot(x_numerical, p_numerical, label=str(dir))
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Pressure, $p$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'phloem_pressure.svg')
				plt.close()
				
				plt.figure(9)
				plt.plot(x_numerical, v0_numerical, label=str(dir))
				plt.plot(x_numerical, gradU_numerical, label=str(dir))
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Boundary velocity comparison', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(subdir + 'boundary_velocity_comaprison.svg')
				plt.close()
				
				plt.figure(10)
				plt.plot(x_numerical, u_numerical, label=str(dir))
				plt.xlabel('Distance along phloem tube', fontsize=16)
				plt.ylabel('Velocity, $u$', fontsize=16)
				plt.xlim(0,max(x_numerical))
				#plt.ylim(0,2.5)
				plt.legend(title=parameter_regime, fontsize=14, title_fontsize=16)
				plt.savefig(rootdir + 'comparison_velocity.svg')
				
				continue
				
			## Fluxes
			elif filename.endswith('flux_simulation_data.csv'):
				data = np.array(list(csv.reader(open(filename, 'r'), delimiter=',')))
				
				continue

print('numericalRunTime = ' + str(datetime.now() - numericalStartTime))

