
Paper: Long-distance hormone transport via the phloem 
Authors: Heather Collis, Leah R Band, Markus R Owen
Journal of Theoretical Biology 2023

run_phloem_paper_results: runs simulations for the various model conditions considered in the results section (excluding parameter surveys). Relies on functions developed in run_numerical_step

run_plotting: creates the line graphs seen in the results section
run_barplots/run_barplots_hormone: creates the clustered bar graphs in the results sections

run_systematic_parameter_search: performs the parameter surveys refered to in the results section

run_contour_plots/run_plasmodesmata_contour: creates the contour plots in the results section
