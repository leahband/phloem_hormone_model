import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
import sys
import os
from run_numerical_step import phloem_structure, phloem_model
import csv
from matplotlib import colors
import matplotlib.lines as mlines

numericalStartTime = datetime.now()
print('numericalStartTime = ' + str(numericalStartTime))

myFile = open('Model_results/PAPER_FIGURES/Finalised_figures/plasmodesmata_contour.csv')

csvreader = csv.reader(myFile)

header = next(csvreader)
data_matrix = []
for row in csvreader:
	row = [float(i) for i in row]
	data_matrix.append(row)

data_matrix = np.asarray(data_matrix)

x = [0.0, 0.2, 0.4]
y = [0.1, 0.5, 1.0, 2.5]
z = np.zeros((len(x),len(y)))

rows = 0
for row in x:
	cols = 0
	for col in y:
		value = data_matrix[data_matrix[:, 0]==row]
		value = value[value[:,1]==col]
		z[rows,cols] = value[-1][-1]
		cols += 1
	rows += 1
		

X,Y = np.meshgrid(x,y)
Z = np.transpose(z)

plt.figure()
plt.contourf(X,Y,Z, cmap='bwr_r', levels=np.linspace(0, 1, 11), extend='neither') #vmin=0, vmax=1)
plt.colorbar()
plt.xlabel('sigmas3', fontsize=16)
plt.xlim(0,0.4)
plt.ylabel('betas3', fontsize=16)
plt.ylim(0.25,1.75)
gammas1_line = mlines.Line2D([], [], color='black', marker='', markersize=1, label='gammas1 = 0.16')
cexts3_line = mlines.Line2D([], [], color='black', marker='', markersize=1, label='cexts3 = 0.4')
plt.legend(handles=[gammas1_line, cexts3_line])
#plt.show()
plt.plot(0.1, 1.5, 'kx')
plt.annotate('high', (0.11, 1.5))
plt.plot(0.2, 1.0, 'kx')
plt.annotate('mid', (0.21, 1.0))
plt.plot(0.3, 0.5, 'kx')
plt.annotate('low', (0.31, 0.5))
plt.savefig('Model_results/PAPER_FIGURES/Finalised_figures/plasmodesmata_contour_plot.svg')
plt.close()


print('numericalRunTime = ' + str(datetime.now() - numericalStartTime))
